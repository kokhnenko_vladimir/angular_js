﻿angular.module("ratesModule", [])
    .controller("contrRates", function ($scope, $http) {
        $http.get("https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5")
            .then(
                res => {
                    $scope.rates = res.data;
                    $scope.bank = "privatbank.ua";
                });
    });