const http = require('http');
const fs = require('fs');
const PORT = '80';
const HOST = '127.0.0.1';

http.createServer(serverCore)
    .listen(PORT, HOST, () => {
        console.log("Server listen port " + PORT);
    });

function serverCore(request, response) {

    let url = decodeURI(request.url.substr(1));
    let method = request.method.toUpperCase();
    console.log(method + ' ' + url);

    switch (url) {
        case '':
        case 'index.html':
            pipeFile('index.html', 'text/html', response);
            break;

        case 'css/styles.css':
            pipeFile('css/styles.css', 'text/css', response);
            break;

        case 'favicon.ico':
            pipeFile('img/icon16.png', 'image/png', response);
            break;

        case 'node_modules/angular/angular.min.js':
            pipeFile('node_modules/angular/angular.min.js', 'text/javascript', response);
            break;

        case 'js/controllers/ratesController.js':
            pipeFile('js/controllers/ratesController.js', 'text/javascript', response);
            break;

        case 'node_modules/angular/angular.min.js.map':
            pipeFile('node_modules/angular/angular.min.js.map', 'text/javascript', response);
            break;

        default:
            pipeFile('index.html', 'text/html', response);
            break;
    }
}


async function pipeFile(name, type, response) {

    var f = fs.createReadStream(name);

    f.on('open', () => {
        response.writeHead(200, {
            "Content-Type": type,
            "Connection": "close"
        });
        f.pipe(response);
    });
}